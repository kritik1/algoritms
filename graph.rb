require "test/unit"

def paths_between(graph, node1, node2)
  dict = graph.each_with_object({}) do |route, h|
    h[route.first] ||= []
    h[route.first].push(route.last)
  end

  next_path(dict, node1, node2, [[node1]])
end

def next_path(dict, start, finish, routes)
  new_routes = []
  done = []
  # to_a needed to fix no route found
  dict[start].to_a.map do |endpoint|
    routes.each do |route|
      next if route.include?(endpoint) # prevents loops
      route_local = route.dup.push(endpoint)
      if endpoint == finish
        done << route_local
      else
        new_routes << route_local
      end
    end

    if endpoint != finish && new_routes.any?
      done |= next_path(dict, endpoint, finish, new_routes)
    end
  end


  done
end


class StringExtensionTest < Test::Unit::TestCase
  def test_mine
    graph = [
      [1, 2], [2, 3],  [3, 4], [2, 4], [1, 4],
    ]
    returns = paths_between(graph, 1, 4)
    should  = [[1,4],[1,2,3,4],[1,2,4]]

    do_check(returns,should)
  end

  def test_task
    graph = [[1, 2], [2, 3], [3, 4], [2, 4]]
    node1 = 1
    node2 = 4
    returns = paths_between(graph, node1, node2)
    should  = [[1,2,3,4], [1, 2, 4]]
    do_check(returns,should)
  end

  def test_with_route_and_no_route
    graph = [[1, 2], [2, 3], [3, 4], [2, 4], [1,  5]]
    returns = paths_between(graph, 1, 4)
    should  = [[1,2,3,4], [1, 2, 4]]
    do_check(returns,should)
  end

  def test_no_route
    graph = [[1, 2], [1, 3]]
    returns = paths_between(graph, 1, 4)
    should  = []
    do_check(returns,should)
  end

  def test_no_starting
    graph = [[1, 2], [1, 3]]
    returns = paths_between(graph, 2, 4)
    do_check(returns,[])
  end

  def test_no_loop
    graph = [[1, 2], [2, 3],  [3, 4], [2, 4], [1, 4], [2,1], [2,1]]
    returns = paths_between(graph, 1, 4)
    should  = [[1, 2, 3, 4], [1, 2, 4], [1, 4]]
    do_check(returns,should)
  end

  def test_ends_and_continues
    graph   = [[1, 2], [1, 3], [2, 4], [3, 4], [4, 5], [4, 6], [6, 5]]
    returns = paths_between(graph, 1, 5)
    should  = [[1, 2, 4, 5], [1, 2, 4, 6, 5], [1, 3, 4, 5], [1, 3, 4, 6, 5]]
    do_check(returns,should)
  end


  def do_check(a1,a2)
    assert(a1.sort.to_s == a2.sort.to_s)
  end
  private :do_check
end
